#### Calculadora de ecuaciones diferenciales

A continuación, verás solo un pequeño demo de una calculadora para resolver ecuaciones diferenciales.


Demo: 

https://ufps.s3.us-east-2.amazonaws.com/ecuaciones/index.html


![Welcome](images/img-1.png)


![Calculator](images/img-2.png)
![Calculator](images/img-3.png)


Hecho con ♥ por [Jose Florez, Frank Moncada](https://joseflorez.co)
